# -*- coding: utf-8 -*-
import MySQLdb
import sys
import PyQt4
import PyQt4.uic
from PyQt4 import QtGui
from PyQt4.QtGui import *
from PyQt4.QtCore import *
from PyQt4 import QtGui, QtCore

import datetime
import paramiko
import time
import urllib2
from xml.dom import minidom
import re
from ConfigParser import ConfigParser
import ctypes
import ast

class MyThread(QtCore.QThread):
    def __init__(self, cpeid, parameters, env_id, scenario_name, web_url):
        QThread.__init__(self)
        self.cpeid = cpeid
        self.parameters = parameters
        self.env_id = env_id
        self.scenario_name = scenario_name
        self.web_url = web_url

    trigger = QtCore.pyqtSignal(str)

    def setup(self, cpe_response):
        self.thread_no = cpe_response

    def check_cpe_online(self):
        web_url = self.web_url
        cpeid = self.cpeid
        env_id = str(time.time())
        url = web_url +\
              '/live/CPEManager/getCPEScenarioProcess?AJAXactivateScenarioNow=1&cpeid=' + \
              cpeid + \
              '&refresh=Refresh&=Run%20Now&=Run%20Scheduled&=Delete%20Finished&sc_filter=&sc_from=-1d&sc_to=&envId=' + \
              env_id + \
              '&interval=&int:restarts=&methodName=GetRPCMethods&multistep_scid=&parameters=%7B%7D'

        urllib2.urlopen(url)

        url = web_url + '/live/CPEManager/getCPELastResult?cpeid=%s&envid=%s&rnd=39196&sc_from=-1d' % (cpeid, env_id)
        current_time = time.time()
        is_online = False

        while time.time() < current_time + 10:
            response = urllib2.urlopen(url).read()
            if 'no result' in response:
                pass
            else:
                is_online = True
                break
        return is_online

    def run(self):
        if self.check_cpe_online():
            cpeid = self.cpeid
            parameters = self.parameters
            env_id = str(time.time())
            scenario_name = self.scenario_name
            web_url = self.web_url

            if scenario_name == 'SetParameterValues':
                parameters = ast.literal_eval(parameters)  # Конвертируем входные параметры в словарь (dict)

                parameters_url_encode = ''

                for key in parameters:
                    parameters_url_encode += "'" + str(key) + "'" + "%3A" + "'" + str(parameters[key]) + "'" + '%2C'

                # parameters = 'InternetGatewayDevice.ManagementServer.PeriodicInformInterval\'' + '%3A' + '3000'

                url = web_url + \
                  '/live/CPEManager/getCPEScenarioProcess?AJAXactivateScenarioNow=1&cpeid=' + \
                  cpeid + \
                  '&refresh=Refresh&=Run%20Now&=Run%20Scheduled&=Delete%20Finished&sc_filter=&sc_from=-1d&sc_to=&envId=' + \
                  env_id + \
                  '&interval=&int:restarts=&methodName=' + \
                  scenario_name + \
                  '&multistep_scid=&parameters=%7B' + \
                  parameters_url_encode + \
                  '%7D&=%3E%3E&newStoreId=&selStoredSC=&LoadSC=Load&DelSC=Delete&StoreSC=Store'
            else:
                url = web_url + \
                      '/live/CPEManager/getCPEScenarioProcess?AJAXactivateScenarioNow=1&cpeid=' + \
                      cpeid + \
                      '&refresh=Refresh&=Run%20Now&=Run%20Scheduled&=Delete%20Finished&sc_filter=&sc_from=-1d&sc_to=&envId=' + \
                      env_id + \
                      '&interval=&int:restarts=&methodName=' + \
                      scenario_name + \
                      '&multistep_scid=&parameters=%5Bu%27' + \
                      parameters + \
                      '%27%5D&=%3E%3E&newStoreId=&selStoredSC=&LoadSC=Load&DelSC=Delete&StoreSC=Store'
            time.sleep(1)
            urllib2.urlopen(url)

            url = web_url + '/live/CPEManager/getCPELastResult?cpeid=%s&envid=%s&rnd=39196&sc_from=-1d' % (cpeid, env_id)
            current_time = time.time()
            while time.time() < current_time + 20:
                response = urllib2.urlopen(url).read()
                if 'no result' in response:
                    time.sleep(1)
                else:
                    break

            response = urllib2.urlopen(url).read()
            self.trigger.emit(response)
        else:
            self.trigger.emit(u'---------------- Модем offline -----------------')

class HelperGUI():
    def __init__(self):

        self.config = ConfigParser()
        self.config.read('config.ini')

        #mysql configuration for ACS db
        self.DBHOSTACS = self.config.get('ACS_DB', 'acs_db_ip')
        self.DBPORTACS = self.config.getint('ACS_DB', 'acs_db_port')
        self.DBUSERACS = self.config.get('ACS_DB', 'acs_db_user')
        self.DBPASSACS = self.config.get('ACS_DB', 'acs_db_pass')
        self.DBNAMEACS = self.config.get('ACS_DB', 'acs_db_name')

        #mysql configuration for dhcp db
        self.DBHOSTDHCP = self.config.get('DHCP_DB', 'dhcp_db_ip')
        self.DBPORTDHCP = self.config.getint('DHCP_DB', 'dhcp_db_port')
        self.DBUSERDHCP = self.config.get('DHCP_DB', 'dhcp_db_user')
        self.DBPASSDHCP = self.config.get('DHCP_DB', 'dhcp_db_pass')
        self.DBNAMEDHCP = self.config.get('DHCP_DB', 'dhcp_db_name')

        #load xml with widgets from *.ui file, which generated by PyQt Designer
        self.ui = PyQt4.uic.loadUi(self.config.get('OTHER', 'path_to_gui'))
        #then set components visible
        self.ui.show()
        #add action to button click
        self.ui.pushButton.clicked.connect(self.handle_button_service_search)
        self.ui.pushButton_2.clicked.connect(self.handle_button_search_ip_address)
        self.ui.pushButton_3.clicked.connect(self.check_last_state_result)
        self.ui.pushButton_4.clicked.connect(self.save_pool)
        self.ui.pushButton_5.clicked.connect(self.pool_search)
        self.ui.pushButton_6.clicked.connect(self.dhcp_server_restart)
        self.ui.pushButton_7.clicked.connect(self.ip_list_sort)
        self.ui.pushButton_8.clicked.connect(self.handle_button_search_cpeid)
        self.ui.pushButton_9.clicked.connect(self.start_threads)

        #add pretty name to column of table
        self.ui.tableWidget.setHorizontalHeaderLabels([u'cid',
                                                       u'Номер/логин',
                                                       u'Ожидающий номер/логин',
                                                       u'Пароль',
                                                       u'Ожидающий пароль',
                                                       u'Состояние',
                                                       u'Тип сервиса'])

        self.ui.tableWidget_2.setHorizontalHeaderLabels([u'IP',
                                                         u'Pool',
                                                         u'MAC-адрес',
                                                         u'Лизинг (старт)',
                                                         u'Лизинг (стоп)',
                                                         u'Опция 82'])

        self.ui.tableWidget_3.setHorizontalHeaderLabels([u'CPEID',
                                                         u'cid',
                                                         u'Первый отклик',
                                                         u'Последний отклик',
                                                         u'Модель',
                                                         u'Версия прошивки',
                                                         u'Опция 82'])

        #now size of columns dependent from size of content
        self.ui.tableWidget.resizeColumnsToContents()
        self.ui.tableWidget_2.resizeColumnsToContents()
        self.ui.tableWidget_3.resizeColumnsToContents()
        self.ui.tableWidget_4.resizeColumnsToContents()
        self.ui.tableWidget.keyPressEvent = self.delete_item_event
        self.ui.tableWidget_2.keyPressEvent = self.delete_item_event_2
        self.ui.tableWidget_3.keyPressEvent = self.delete_item_event_cpeid

        self.ui.radioButton.toggled.connect(self.leasing_radio_button_event)
        self.ui.radioButton_2.toggled.connect(self.blacklist_radio_button_event)

        self.ui.tabWidget.setCurrentIndex(0)
        self.ui.tabWidget.currentChanged.connect(self.open_pool)  # changed event
        self.ui.comboBox.currentIndexChanged.connect(self.methods_select_changed)

    def start_threads(self):
        self.threads = []              # this will keep a reference to threads

        cpeid = str(self.ui.lineEdit_5.text())
        parameters = str(self.ui.plainTextEdit_4.toPlainText())
        env_id = str(time.time())
        scenario_name = str(self.ui.comboBox.currentText())
        web_url = self.config.get('OTHER', 'web_interface')

        thread = MyThread(cpeid=cpeid, parameters=parameters, env_id=env_id, scenario_name=scenario_name, web_url=web_url)    # create a thread

        thread.trigger.connect(self.update_text)  # connect to it's signal
        thread.setup('Empty')            # just setting up a parameter
        thread.start()             # start the thread
        self.threads.append(thread) # keep a reference

    def update_text(self, thread_no):
        self.ui.textBrowser.setText(str(thread_no))

    def methods_select_changed(self):
        if str(self.ui.comboBox.currentText()) == 'SetParameterValues':
            self.ui.plainTextEdit_4.clear()
            self.ui.plainTextEdit_4.insertPlainText("{'InternetGatewayDevice.ManagementServer.URL' : 'http://10.254.0.20:7547/web/tr069'}")
        else:
            self.ui.plainTextEdit_4.clear()
            self.ui.plainTextEdit_4.insertPlainText("InternetGatewayDevice.Services.")

    # If it is a leasing table
    def leasing_radio_button_event(self):
        self.ui.tableWidget_2.setRowCount(0)
        self.ui.tableWidget_2.setColumnCount(6)
        self.ui.tableWidget_2.setHorizontalHeaderLabels([u'IP',
                                                         u'Pool',
                                                         u'MAC-адрес',
                                                         u'Лизинг (старт)',
                                                         u'Лизинг (стоп)',
                                                         u'Опция 82'])
        self.ui.tableWidget_2.resizeColumnsToContents()

    def blacklist_radio_button_event(self):
        self.ui.tableWidget_2.setRowCount(0)
        self.ui.tableWidget_2.setColumnCount(4)
        self.ui.tableWidget_2.setHorizontalHeaderLabels([u'MAC-адрес',
                                                         u'Опция 82',
                                                         u'Причина',
                                                         u'Время'])
        self.ui.tableWidget_2.resizeColumnsToContents()

    @staticmethod
    def check_user_entry(user_entry):
        user_entry = str(user_entry)
        if re.search(r'^\d{1,3}\.\d{1,3}\.\d{1,3}\.[01]{1}$', user_entry):  # looks like pool address
            return "pool"
        elif re.search(r'^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$', user_entry):  # looks like ip address
            return "ip"
        elif re.search(r'^[A-Fa-f0-9\.\:\-]{1,30}$', user_entry):  # looks like mac address
            return "mac"
        else:
            return "opt82"

    @staticmethod
    def check_user_entry_cpeid_search(user_entry):
        user_entry = str(user_entry)
        if re.search(r'^\d{1,10}$', user_entry):
            return "cid"
        if re.search(r'^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$', user_entry):
            return "ip"
        elif re.search(r'^[A-Fa-f0-9\.\:\-]{1,30}$', user_entry):
            return "mac"
        elif re.search(r'^\+\d{12}$', user_entry):
            return "phone"
        elif re.search(r'@beltel.by', user_entry):
            return "login"
        else:
            return "opt82"

    @staticmethod
    def make_pretty_mac(mac):
        mac = str(mac)
        mac = mac.replace("-", "")
        mac = mac.replace(".", "")
        mac = mac.replace(":", "")
        mac = mac.upper()
        return mac

    @staticmethod
    def make_pretty_mac_version_2(mac):
        mac = str(mac)
        mac = mac.replace(".", "")
        mac = mac.replace(":", "")
        mac = mac.replace("-", "")
        mac = mac[0:2] + '-' + mac[2:4] + '-' + mac[4:6] + '-' + mac[6:8] + '-' + mac[8:10] + '-' + mac[10:12]
        mac = mac.lower()
        print mac
        return mac

    def handle_button_search_ip_address(self):
        if self.ui.radioButton.isChecked():
            if len(self.ui.lineEdit_2.text()) != 0:
                #get text from edit components
                where_what = ''
                user_entry = self.ui.lineEdit_2.text()
                #return by what we will find (mac, ip, pool or opt82)
                view_user_entry = self.check_user_entry(user_entry)
                result = ''

                #
                # MAC address handler
                #

                if view_user_entry == 'mac':
                    user_entry = self.make_pretty_mac(user_entry)
                    where_what = 'hex(mac)'
                    if len(user_entry) != 12:
                        PyQt4.QtGui.QMessageBox.information(self.ui, u"Сообщение",
                                                            u"MAC-адрес содержит меньше 12 символов",
                                                            PyQt4.QtGui.QMessageBox.Ok)
                        return None

                    #Cut zero symbol at start of the MAC
                    for symbol in user_entry:
                        if symbol != '0':
                            break
                        else:
                            user_entry = user_entry[1:]

                #
                # IP, pool and opt82 address handler
                #

                elif view_user_entry == 'ip':
                    where_what = 'inet_ntoa(ip)'

                elif view_user_entry == 'pool':
                    where_what = 'inet_ntoa(pool)'

                elif view_user_entry == 'opt82':
                    where_what = 'circutId'
                print where_what
                #sql query for execute by mysql server
                sqlquery =  "select inet_ntoa(ip)," \
                            " inet_ntoa(pool)," \
                            " hex(mac)," \
                            " from_unixtime(leaseStarttime)," \
                            " from_unixtime(leasetime)," \
                            " circutId " \
                            " from leasing where %s like '%s'" % (where_what, user_entry)

                try:
                    #connect to database
                    con = MySQLdb.connect(host=self.DBHOSTDHCP,
                                          port=self.DBPORTDHCP,
                                          user=self.DBUSERDHCP,
                                          passwd=self.DBPASSDHCP,
                                          db=self.DBNAMEDHCP)
                    cur = con.cursor()
                    cur.execute(sqlquery)
                    #get result in tuple, where each element tuple too
                    result = cur.fetchall()

                except Exception as err:
                    PyQt4.QtGui.QMessageBox.information(self.ui, u"Error",
                                                                 u"%s" % err,
                                                                 PyQt4.QtGui.QMessageBox.Ok)
                else:
                    cur.close()

                #current column (row increment automatically by method setItem)
                column_count = 0
                #add nessessary count of rows
                self.ui.tableWidget_2.setRowCount(len(result))
                for row in result:
                    for column in row:
                        column = str(column)
                        #add item to table (note: row increment automatically by method setItem)
                        self.ui.tableWidget_2.setItem(0, column_count, PyQt4.QtGui.QTableWidgetItem(column))
                        #now size of columns dependent from size of content
                        self.ui.tableWidget_2.resizeColumnsToContents()
                        column_count += 1
                if len(result) == 0:
                    PyQt4.QtGui.QMessageBox.information(self.ui, u"Сообщение",
                                                        u"Ничего не найдено",
                                                        PyQt4.QtGui.QMessageBox.Ok)

        #for blacklist search
        elif self.ui.radioButton_2.isChecked():
            if len(self.ui.lineEdit_2.text()) != 0:
                #get text from edit components
                macaddress = self.ui.lineEdit_2.text()
                macaddress = str(macaddress)
                macaddress = macaddress.replace("-", "")
                macaddress = macaddress.replace(".", "")
                macaddress = macaddress.upper()
                result = ''
                if len(macaddress) != 12:
                    PyQt4.QtGui.QMessageBox.information(self.ui, u"Сообщение",
                                                        u"MAC-адрес содержит меньше 12 символов",
                                                        PyQt4.QtGui.QMessageBox.Ok)
                    return None

                for symbol in macaddress:
                    if symbol != '0':
                        break
                    else:
                        macaddress = macaddress[1:]

                #sql query for execute by mysql server
                sqlquery =  "select hex(mac), " \
                            "circutId, " \
                            "reason, " \
                            "ts " \
                            "from blacklists where hex(mac) like '%s'" % macaddress

                try:
                    #connect to database
                    con = MySQLdb.connect(host=self.DBHOSTDHCP,
                                          port=self.DBPORTDHCP,
                                          user=self.DBUSERDHCP,
                                          passwd=self.DBPASSDHCP,
                                          db=self.DBNAMEDHCP)
                    cur = con.cursor()
                    cur.execute(sqlquery)
                    #get result in tuple, where each element tuple too
                    result = cur.fetchall()

                except Exception as err:
                    PyQt4.QtGui.QMessageBox.information(self.ui, u"Error",
                                                                u"%s" % err,
                                                                PyQt4.QtGui.QMessageBox.Ok)
                else:
                    cur.close()

                #current column (row increment automatically by method setItem)
                column_count = 0
                #add nessessary count of rows
                self.ui.tableWidget_2.setRowCount(len(result))
                for row in result:
                    for column in row:
                        column = str(column)
                        #add item to table (note: row increment automatically by method setItem)
                        self.ui.tableWidget_2.setItem(0, column_count, PyQt4.QtGui.QTableWidgetItem(column))
                        #now size of columns dependent from size of content
                        self.ui.tableWidget_2.resizeColumnsToContents()
                        column_count += 1
                if len(result) == 0:
                    PyQt4.QtGui.QMessageBox.information(self.ui, u"Сообщение",
                                                        u"Ничего не найдено",
                                                        PyQt4.QtGui.QMessageBox.Ok)

    def handle_button_service_search(self):
        cid_search = False
        if len(self.ui.lineEdit.text()) != 0:

            user_entry = str(self.ui.lineEdit.text())
            if re.search(r'^\+\d{12}$', user_entry):
                user_entry = user_entry[4:]
                cid_from_tm = self.get_cid_by_number_from_tm(user_entry)
                user_entry = '+375' + user_entry
                user_entry = '%' + user_entry + '%'

            elif re.search(r'@beltel.by', user_entry):
                cid_from_tm = self.get_cid_by_login_from_tm(self.ui.lineEdit.text())
                user_entry = '%' + user_entry + '%'

            sql_query = "select cid," \
                        " value1," \
                        " value1pending," \
                        " value2," \
                        " value2pending," \
                        " realStatusDetails," \
                        " servicetype" \
                        " from AXServiceTable where value1 like '%s' or value1Pending like '%s'" % (user_entry, user_entry)

            if re.search(r'^\d{1,10}$', user_entry):
                sql_query = "select cid," \
                            " value1," \
                            " value1pending," \
                            " value2," \
                            " value2pending," \
                            " realStatusDetails," \
                            " servicetype" \
                            " from AXServiceTable where cid like '%s'" % user_entry
                cid_search = True
                cid_from_tm = '0'


            #connect to database
            con = MySQLdb.connect(host=self.DBHOSTACS,
                                  port=self.DBPORTACS,
                                  user=self.DBUSERACS,
                                  passwd=self.DBPASSACS,
                                  db=self.DBNAMEACS)
            cur = con.cursor()
            cur.execute(sql_query)
            #get result in tuple, where each element tuple too
            result = cur.fetchall()
            #current column (row increment automatically by method setItem)
            column_count = 0
            #add nessessary count of rows
            self.ui.tableWidget.setRowCount(len(result))
            for row in result:
                for column in row:
                    column = str(column)
                    #add item to table (note: row increment automatically by method setItem)
                    self.ui.tableWidget.setItem(0, column_count, PyQt4.QtGui.QTableWidgetItem(column))
                    #now size of columns dependent from size of content
                    self.ui.tableWidget.resizeColumnsToContents()

                    column_count += 1
            if len(result) == 0:
                PyQt4.QtGui.QMessageBox.information(self.ui, "Message", "Empty set", PyQt4.QtGui.QMessageBox.Ok)
            else:

                #search into the table a row with a cid like into TM
                for i in range(self.ui.tableWidget.rowCount()):
                    if cid_search:
                        if self.ui.tableWidget.item(i, 1).text() != 'None':
                            if self.ui.tableWidget.item(i, 6).text() == 'HSI':
                                cid_from_tm = self.get_cid_by_login_from_tm(self.ui.tableWidget.item(i, 1).text())
                            elif self.ui.tableWidget.item(i, 6).text() == 'VOIP':
                                cid_from_tm = self.get_cid_by_number_from_tm(self.ui.tableWidget.item(i, 1).text()[4:])
                        elif self.ui.tableWidget.item(i, 1).text() == 'None':  # try to catch infromation from pending values
                            if self.ui.tableWidget.item(i, 6).text() == 'HSI':
                                cid_from_tm = self.get_cid_by_login_from_tm(self.ui.tableWidget.item(i, 2).text())
                            elif self.ui.tableWidget.item(i, 6).text() == 'VOIP':
                                cid_from_tm = self.get_cid_by_number_from_tm(self.ui.tableWidget.item(i, 2).text()[4:])
                        else:
                            cid_from_tm = '0'

                    if self.ui.tableWidget.item(i, 0).text() == cid_from_tm:
                        for c in range(self.ui.tableWidget.columnCount()):
                            self.ui.tableWidget.item(i, c).setBackground(PyQt4.QtGui.QColor(92, 188, 105))
                    else:
                        for c in range(self.ui.tableWidget.columnCount()):
                            self.ui.tableWidget.item(i, c).setBackground(PyQt4.QtGui.QColor(237, 53, 29))

    def handle_button_search_cpeid(self):
        #get text from edit components
        where_what = ''
        user_entry = self.ui.lineEdit_4.text()
        #return by what we will find (mac, ip and etc.)
        view_user_entry = self.check_user_entry_cpeid_search(user_entry)
        result = ''

        #
        # MAC address handler
        #

        if view_user_entry == 'mac':
            user_entry = self.make_pretty_mac_version_2(user_entry)
            where_what = 'cid2'
            if len(user_entry) != 17:
                PyQt4.QtGui.QMessageBox.information(self.ui, u"Сообщение",
                                                    u"MAC-адрес содержит меньше 12 символов",
                                                    PyQt4.QtGui.QMessageBox.Ok)
                return None

        #
        # IP, pool and opt82 address handler
        #

        elif view_user_entry == 'cid':
            where_what = 'cid'

        elif view_user_entry == 'ip':
            where_what = 'ip'

        elif view_user_entry == 'phone' or view_user_entry == 'login':
            where_what = 'props'
            user_entry = '%' + user_entry + '%'

        elif view_user_entry == 'opt82':
            where_what = 'scProps'
            user_entry = '%' + user_entry + '%'
        print where_what
        #sql query for execute by mysql server
        model = "substr(substr(props,instr(props, '\"I.DI.PC\": \"')+11),1,instr(substr(props,instr(props, '\"I.DI.PC\": \"')+11),'\",'))"
        opt82 = "substr(substr(scprops,instr(scprops, '\"option82\": \"')+13),1,instr(substr(scprops,instr(scprops, '\"option82\": \"')+14),'\"'))"
        sqlquery = "select cpeid, cid, firstMsg, lastMsg, %s, version, %s from CPEManager_CPEs where %s like '%s'" % (model, opt82, where_what, user_entry)

        try:
            #connect to database
            con = MySQLdb.connect(host=self.DBHOSTACS,
                                  port=self.DBPORTACS,
                                  user=self.DBUSERACS,
                                  passwd=self.DBPASSACS,
                                  db=self.DBNAMEACS)
            cur = con.cursor()
            cur.execute(sqlquery)
            #get result in tuple, where each element tuple too
            result = cur.fetchall()

        except Exception as err:
            PyQt4.QtGui.QMessageBox.information(self.ui, u"Error",
                                                         u"%s" % err,
                                                         PyQt4.QtGui.QMessageBox.Ok)
        else:
            cur.close()

        #current column (row increment automatically by method setItem)
        column_count = 0
        row_count = 0
        new_row = True
        #add nessessary count of rows
        self.ui.tableWidget_3.setRowCount(len(result))
        result = sorted(result, key=lambda x: x[3], reverse=True)
        for row in result:
            for column in row:
                if isinstance(column, str) is False:
                    column = datetime.datetime.fromtimestamp(column).strftime('%Y-%m-%d %H:%M:%S')
                column = str(column)
                #add item to table (note: row increment automatically by method setItem)
                if new_row:
                    # create an cell widget
                    link = QLabel(self.ui.tableWidget_3)
                    link.setOpenExternalLinks(True)
                    link.setTextInteractionFlags(Qt.TextBrowserInteraction)
                    link.setText(u'<html><head/><body><p><a href="http://86.57.153.107:9672/live/CPEManager/manage_scenario?cpeid=%s&amp;activateRefr=1"><span style=" text-decoration: underline; color:#0000ff;">%s</span></a></p></body></html>' % (column,column))
                    self.ui.tableWidget_3.setCellWidget(row_count, 0, link)
                    new_row = False
                else:
                    self.ui.tableWidget_3.setItem(0, column_count, PyQt4.QtGui.QTableWidgetItem(column))
                #now size of columns dependent from size of content
                self.ui.tableWidget_3.resizeColumnsToContents()
                column_count += 1
            row_count += 1
            new_row = True

        if len(result) == 0:
            PyQt4.QtGui.QMessageBox.information(self.ui, u"Сообщение",
                                                u"Ничего не найдено",
                                                PyQt4.QtGui.QMessageBox.Ok)

    def delete_item_event(self, event):
        #if delete key pressed:
        if event.key() == PyQt4.QtCore.Qt.Key_Delete:
            #current number of row which is selected
            row = self.ui.tableWidget.currentItem().row()
            current_cid = self.ui.tableWidget.item(row, 0).text()
            service_type = self.ui.tableWidget.item(row, 6).text()
            #show a dialog with question (return True if "yes" and False if "no"):
            if self.delete_dialog(current_cid) is True and len(current_cid) != 0 and str(current_cid).isdigit():
                #sql query for execute by mysql server
                sql_query = "delete from AXServiceTable where cid like '%s' and servicetype like '%s'" % (current_cid, service_type)
                #connect to database
                con = MySQLdb.connect(host=self.DBHOSTACS,
                                      port=self.DBPORTACS,
                                      user=self.DBUSERACS,
                                      passwd=self.DBPASSACS,
                                      db=self.DBNAMEACS)
                cur = con.cursor()
                cur.execute(sql_query)
                #delete row
                self.ui.tableWidget.removeRow(row)

        return PyQt4.QtGui.QTableWidget.keyPressEvent(self.ui.tableWidget, event)

    def delete_item_event_2(self, event):
        if self.ui.radioButton_2.isChecked():
            #if delete key pressed:
            if event.key() == PyQt4.QtCore.Qt.Key_Delete:
                #current number of row which is selected
                row = self.ui.tableWidget_2.currentItem().row()
                current_mac = self.ui.tableWidget_2.item(row, 0).text()
                #show a dialog with question (return True if "yes" and False if "no"):
                if self.delete_dialog_2(current_mac) is True:
                    #sql query for execute by mysql server
                    sql_query = "delete from blacklists where hex(mac) like '%s'" % current_mac
                    #connect to database
                    con = MySQLdb.connect(host=self.DBHOSTDHCP,
                                          port=self.DBPORTDHCP,
                                          user=self.DBUSERDHCP,
                                          passwd=self.DBPASSDHCP,
                                          db=self.DBNAMEDHCP)
                    cur = con.cursor()
                    cur.execute(sql_query)
                    #delete row
                    self.ui.tableWidget_2.removeRow(row)
        elif self.ui.radioButton.isChecked():
            #if delete key pressed:
            if event.key() == PyQt4.QtCore.Qt.Key_Delete:
                #current number of row which is selected
                row = self.ui.tableWidget_2.currentItem().row()
                current_ip = self.ui.tableWidget_2.item(row, 0).text()
                current_mac = self.ui.tableWidget_2.item(row, 2).text()
                #show a dialog with question (return True if "yes" and False if "no"):
                if self.delete_dialog_2(current_mac) is True:
                    #sql query for execute by mysql server
                    sql_query = "delete from leasing where hex(mac) like '%s' and inet_ntoa(ip) like '%s'" % (current_mac, current_ip)
                    #connect to database
                    con = MySQLdb.connect(host=self.DBHOSTDHCP,
                                          port=self.DBPORTDHCP,
                                          user=self.DBUSERDHCP,
                                          passwd=self.DBPASSDHCP,
                                          db=self.DBNAMEDHCP)
                    cur = con.cursor()
                    cur.execute(sql_query)
                    #delete row
                    self.ui.tableWidget_2.removeRow(row)

            return PyQt4.QtGui.QTableWidget.keyPressEvent(self.ui.tableWidget_2, event)

    def delete_item_event_cpeid(self, event):
            #if delete key pressed:
            if event.key() == PyQt4.QtCore.Qt.Key_Delete:
                #current number of row which is selected
                row = self.ui.tableWidget_3.currentItem().row()
                current_cpeid = self.ui.tableWidget_3.item(row, 0).text()
                #show a dialog with question (return True if "yes" and False if "no"):
                if self.delete_dialog_cpeid(current_cpeid) is True and len(current_cpeid) != 0:
                    #sql query for execute by mysql server
                    sql_query = "delete from CPEManager_CPEs where cpeid like '%s'" % current_cpeid
                    #connect to database
                    con = MySQLdb.connect(host=self.DBHOSTACS,
                                          port=self.DBPORTACS,
                                          user=self.DBUSERACS,
                                          passwd=self.DBPASSACS,
                                          db=self.DBNAMEACS)
                    cur = con.cursor()
                    cur.execute(sql_query)
                    #delete row
                    self.ui.tableWidget_3.removeRow(row)

            return PyQt4.QtGui.QTableWidget.keyPressEvent(self.ui.tableWidget_3, event)

    def delete_dialog(self, cid):
        quit_msg = u"Вы действительно хотите удалить запись с cid %s?" % cid
        reply = PyQt4.QtGui.QMessageBox.question(self.ui, 'Message', quit_msg, PyQt4.QtGui.QMessageBox.Yes, PyQt4.QtGui.QMessageBox.No)

        if reply == PyQt4.QtGui.QMessageBox.Yes:
            return True
        else:
            return False

    def delete_dialog_2(self, mac):
        quit_msg = u"Вы действительно хотите удалить запись с MAC %s?" % mac
        reply = PyQt4.QtGui.QMessageBox.question(self.ui, 'Message', quit_msg, PyQt4.QtGui.QMessageBox.Yes, PyQt4.QtGui.QMessageBox.No)

        if reply == PyQt4.QtGui.QMessageBox.Yes:
            return True
        else:
            return False

    def delete_dialog_cpeid(self, cpeid):
        quit_msg = u"Вы действительно хотите удалить запись с cpeid %s?" % cpeid
        reply = PyQt4.QtGui.QMessageBox.question(self.ui, 'Message', quit_msg, PyQt4.QtGui.QMessageBox.Yes, PyQt4.QtGui.QMessageBox.No)

        if reply == PyQt4.QtGui.QMessageBox.Yes:
            return True
        else:
            return False

    def check_last_state_result(self):
        host_name = self.config.get('STAGE', 'ip')
        user = self.config.get('AUTH_SSH', 'user_name')
        secret = self.config.get('AUTH_SSH', 'password')
        port = 22

        client = paramiko.SSHClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        client.connect(hostname=host_name, username=user, password=secret, port=port, timeout=100)

        channel = client.get_transport().open_session()
        channel.get_pty()

        channel.exec_command('sudo /opt/scripts/print_last_state.py')
        time.sleep(1)
        channel.send('%s\n' % self.config.get('AUTH_SSH', 'password'))

        while not channel.exit_status_ready():
            pass

        time.sleep(2)

        result = channel.recv(100000)

        result = result[29:]
        result = result.replace("\n", "")
        array_of_string = result.split('\r')[:-1]
        final_list_of_string = []
        for item in array_of_string:
            final_list_of_string.append(item.split('  '))

        self.ui.tableWidget_4.setRowCount(len(final_list_of_string))

        row_count = 0
        for item in final_list_of_string:

            self.ui.tableWidget_4.setItem(row_count, 0, PyQt4.QtGui.QTableWidgetItem(item[0]))
            self.ui.tableWidget_4.setItem(row_count, 1, PyQt4.QtGui.QTableWidgetItem(item[-1].lstrip()))

            row_count += 1
        self.ui.label_5.setText(u"Время последнего запуска: %s" % datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
        #self.ui.tableWidget_4.resizeColumnsToContents()


    def open_pool(self):
        if self.ui.tabWidget.tabText(self.ui.tabWidget.currentIndex()) == 'Pool Editor':
            QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))  # waiting
            try:
                host_name = self.config.get('DHCP', 'dhcp01')
                user = self.config.get('AUTH_SSH', 'user_name')
                secret = self.config.get('AUTH_SSH', 'password')
                port = 22

                client = paramiko.SSHClient()
                client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
                client.connect(hostname=host_name, username=user, password=secret, port=port, timeout=100)

                sftp_client = client.open_sftp()
                remote_file = sftp_client.open('/opt/dhcp/opt/dhcpd/axdrs/pools/pool.txt', 'r')

                data = remote_file.read()

                self.ui.plainTextEdit_2.insertPlainText(str(data))

                row_count = len(str(data).split('\n'))
                if len(str(data).split('\n')[-1]) == 0:
                    row_count -= 1
                self.ui.label_4.setText(u"Общее число записей: %i" % row_count)
                self.ui.pushButton_4.setEnabled(True)
                self.ui.pushButton_7.setEnabled(True)
                QApplication.restoreOverrideCursor()

            except Exception as err:
                PyQt4.QtGui.QMessageBox.information(self.ui, u"Error on %s" % host_name,
                                                             u"%s" % err,
                                                             PyQt4.QtGui.QMessageBox.Ok)
                #traceback.format_exc()
            else:
                remote_file.close()
                client.close()
        else:
            self.ui.plainTextEdit_2.clear()

    def save_pool(self):
        hosts = (self.config.get('DHCP', 'dhcp01'), self.config.get('DHCP', 'dhcp02'))
        user = self.config.get('AUTH_SSH', 'user_name')
        secret = self.config.get('AUTH_SSH', 'password')
        port = 22
        success_host = []

        for host_name in hosts:
            try:

                client = paramiko.SSHClient()
                client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
                client.connect(hostname=host_name, username=user, password=secret, port=port, timeout=100)

                sftp_client = client.open_sftp()
                remote_file = sftp_client.open('/opt/dhcp/opt/dhcpd/axdrs/pools/pool.txt', 'w')
                remote_file.write(self.ui.plainTextEdit_2.toPlainText())
            except Exception as err:
                PyQt4.QtGui.QMessageBox.information(self.ui, u"Error on %s" % host_name,
                                                             u"%s" % err,
                                                             PyQt4.QtGui.QMessageBox.Ok)
                #traceback.format_exc()
            else:
                remote_file.close()
                client.close()
                success_host.append(host_name)

        row_count = len(str(self.ui.plainTextEdit_2.toPlainText()).split('\n'))
        if len(str(self.ui.plainTextEdit_2.toPlainText()).split('\n')[-1]) == 0:
            row_count -= 1
        self.ui.label_4.setText(u"Общее число записей: %i" % row_count)

        PyQt4.QtGui.QMessageBox.information(self.ui, u"Success",
                                                     u"Файл успешно сохранён на:\n%s" % success_host,
                                                     PyQt4.QtGui.QMessageBox.Ok)

    def get_cid_by_number_from_tm(self, number):
        get_request = self.config.get('OTHER', 'tm_api_url') + '?phoneNumber=%s' % number
        response = urllib2.urlopen(get_request).read()

        xmldoc = minidom.parseString(response)
        item = xmldoc.getElementsByTagName('getcid')
        try:
            cid = item[0].childNodes[0].data
        except IndexError:
            cid = 0

        return cid

    def get_cid_by_login_from_tm(self, login):
        get_request = self.config.get('OTHER', 'tm_api_url') + '?loginbyfly=%s' % login
        response = urllib2.urlopen(get_request).read()

        xmldoc = minidom.parseString(response)
        item = xmldoc.getElementsByTagName('getcid')
        try:
            cid = item[0].childNodes[0].data
        except IndexError:
            cid = 0

        return cid

    def check_cpe_online(self):
        web_url = self.config.get('OTHER', 'web_interface')
        cpeid = str(self.ui.lineEdit_5.text())
        env_id = str(time.time())
        url = web_url +\
              '/live/CPEManager/getCPEScenarioProcess?AJAXactivateScenarioNow=1&cpeid=' + \
              cpeid + \
              '&refresh=Refresh&=Run%20Now&=Run%20Scheduled&=Delete%20Finished&sc_filter=&sc_from=-1d&sc_to=&envId=' + \
              env_id + \
              '&interval=&int:restarts=&methodName=GetRPCMethods&multistep_scid=&parameters=%7B%7D'

        urllib2.urlopen(url)

        url = web_url + '/live/CPEManager/getCPELastResult?cpeid=%s&envid=%s&rnd=39196&sc_from=-1d' % (cpeid, env_id)
        current_time = time.time()
        is_online = False

        while time.time() < current_time + 10:
            response = urllib2.urlopen(url).read()
            if 'no result' in response:
                pass
            else:
                is_online = True
                break
        return is_online

    def run_scenario(self):
        if self.check_cpe_online():
            self.ui.progressBar.setValue(0)
            cpeid = str(self.ui.lineEdit_5.text())
            parameters = str(self.ui.plainTextEdit_4.toPlainText())
            env_id = str(time.time())
            scenario_name = str(self.ui.comboBox.currentText())
            web_url = self.config.get('OTHER', 'web_interface')

            if scenario_name == 'SetParameterValues':
                parameters = ast.literal_eval(parameters)  # Конвертируем входные параметры в словарь (dict)

                parameters_url_encode = ''

                for key in parameters:
                    parameters_url_encode += "'" + str(key) + "'" + "%3A" + "'" + str(parameters[key]) + "'" + '%2C'

                # parameters = 'InternetGatewayDevice.ManagementServer.PeriodicInformInterval\'' + '%3A' + '3000'

                print parameters_url_encode

                url = web_url + \
                  '/live/CPEManager/getCPEScenarioProcess?AJAXactivateScenarioNow=1&cpeid=' + \
                  cpeid + \
                  '&refresh=Refresh&=Run%20Now&=Run%20Scheduled&=Delete%20Finished&sc_filter=&sc_from=-1d&sc_to=&envId=' + \
                  env_id + \
                  '&interval=&int:restarts=&methodName=' + \
                  scenario_name + \
                  '&multistep_scid=&parameters=%7B' + \
                  parameters_url_encode + \
                  '%7D&=%3E%3E&newStoreId=&selStoredSC=&LoadSC=Load&DelSC=Delete&StoreSC=Store'
            else:
                url = web_url + \
                      '/live/CPEManager/getCPEScenarioProcess?AJAXactivateScenarioNow=1&cpeid=' + \
                      cpeid + \
                      '&refresh=Refresh&=Run%20Now&=Run%20Scheduled&=Delete%20Finished&sc_filter=&sc_from=-1d&sc_to=&envId=' + \
                      env_id + \
                      '&interval=&int:restarts=&methodName=' + \
                      scenario_name + \
                      '&multistep_scid=&parameters=%5Bu%27' + \
                      parameters + \
                      '%27%5D&=%3E%3E&newStoreId=&selStoredSC=&LoadSC=Load&DelSC=Delete&StoreSC=Store'
            time.sleep(1)
            urllib2.urlopen(url)

            url = web_url + '/live/CPEManager/getCPELastResult?cpeid=%s&envid=%s&rnd=39196&sc_from=-1d' % (cpeid, env_id)
            current_time = time.time()
            while time.time() < current_time + 20:
                response = urllib2.urlopen(url).read()
                if 'no result' in response:
                    time.sleep(1)
                    self.ui.progressBar.setValue(self.ui.progressBar.value() + 5)
                    pass
                else:
                    self.ui.progressBar.setValue(100)
                    break

            response = urllib2.urlopen(url).read()
            self.ui.textBrowser.setText(str(response))
        else:
            self.ui.textBrowser.setText(u'---------------- Модем offline -----------------')

    def pool_search(self):
        newcur = self.ui.plainTextEdit_2.document().find(self.ui.lineEdit_3.text())
        if not newcur.isNull():
            self.ui.plainTextEdit_2.setTextCursor(newcur)
            self.ui.plainTextEdit_2.setFocus()
        else:
            PyQt4.QtGui.QMessageBox.information(self.ui, u"Поиск не дал результата!",
                                                         u"%s не найдено!" % self.ui.lineEdit_3.text(),
                                                         PyQt4.QtGui.QMessageBox.Ok)

    def dhcp_server_restart(self):
        hosts = (self.config.get('DHCP', 'dhcp01'), self.config.get('DHCP', 'dhcp02'))
        user = self.config.get('AUTH_SSH', 'user_name')
        secret = self.config.get('AUTH_SSH', 'password')
        port = 22
        for host_name in hosts:
            try:
                QApplication.setOverrideCursor(QCursor(Qt.WaitCursor))  # waiting
                app.processEvents()  # waiting
                self.ui.plainTextEdit_3.insertPlainText(u'Пробуем перестартовать DHCP-сервер %s...' % host_name)
                client = paramiko.SSHClient()
                client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
                client.connect(hostname=host_name, username=user, password=secret, port=port, timeout=100)

                channel = client.get_transport().open_session()
                channel.get_pty()

                channel.exec_command('sudo /etc/init.d/axess_dhcp restart')
                time.sleep(1)
                channel.send('%s\n' % self.config.get('AUTH_SSH', 'password'))

                while not channel.exit_status_ready():
                    pass

                time.sleep(2)

                result = channel.recv(100000)
                result = result[29:]
                self.ui.plainTextEdit_3.insertPlainText(str(result))

            except Exception as err:
                PyQt4.QtGui.QMessageBox.information(self.ui, u"Error on %s" % host_name,
                                                             u"%s" % err,
                                                             PyQt4.QtGui.QMessageBox.Ok)

            else:
                QApplication.restoreOverrideCursor()

    def ip_list_sort(self):
        f = self.dec_to_ip(self.ip_to_dec())
        self.ui.plainTextEdit_2.clear()
        #insert sorted ip list into plain text
        self.ui.plainTextEdit_2.insertPlainText(str(f))

    def ip_to_dec(self):
        ip_list_dec = []
        ip_list = str(self.ui.plainTextEdit_2.toPlainText()).split('\n')
        line_count = 0
        for ip in ip_list:

            if ip == '':
                continue

            ip = ip.split('.')

            #select net mask
            mask = ip[3][ip[3].find('/'):]

            #ip to decimal format
            ip = int(ip[0])*(256 ** 3) + int(ip[1])*(256 ** 2) + int(ip[2])*256 + int(ip[3][:ip[3].find('/')])

            ip_list_dec.append((ip, mask))
            line_count += 1

        sorted_ip_list = sorted(ip_list_dec,  key=lambda ip_dec: ip_dec[0])

        return sorted_ip_list

    def dec_to_ip(self, ip_list):
        f = ''
        for item in ip_list:
            ip = item[0]
            mask = item[1]
            ip = str(int(ip/(256**3))) + '.' + str(int((ip % (256**3)) / (256**2))) + '.' + str(int((ip % (256**2)) / 256)) + '.' + str(int(ip % 256))
            ip_mask = ip + mask + '\n'
            f += ip_mask
        return f

if __name__ == '__main__':
    myappid = u'mycompany.myproduct.subproduct.version'  # arbitrary string
    ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(myappid)

    #create app object first
    app = QApplication(sys.argv)
    #instance of HelperGUI class
    guiObject = HelperGUI()
    #for object inspection, get all component's name
    #print guiObject.ui.__dict__.keys()
    #main loop
    app.setWindowIcon(QtGui.QIcon('axiros.png'))
    guiObject.ui.setWindowIcon(QtGui.QIcon('axiros.png'))
    sys.exit(app.exec_())
